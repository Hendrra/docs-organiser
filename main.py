from source.organiser import Organiser
from source.parser import parse_arguments


if __name__ == "__main__":
    parsed = parse_arguments()
    organiser = Organiser(parsed=parsed)
    organiser.organise()
