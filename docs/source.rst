source package
==============

Submodules
----------

source.config module
--------------------

.. automodule:: source.config
   :members:
   :undoc-members:
   :show-inheritance:

source.crawler module
---------------------

.. automodule:: source.crawler
   :members:
   :undoc-members:
   :show-inheritance:

source.extractor module
-----------------------

.. automodule:: source.extractor
   :members:
   :undoc-members:
   :show-inheritance:

source.interpreter module
-------------------------

.. automodule:: source.interpreter
   :members:
   :undoc-members:
   :show-inheritance:

source.organiser module
-----------------------

.. automodule:: source.organiser
   :members:
   :undoc-members:
   :show-inheritance:

source.parser module
--------------------

.. automodule:: source.parser
   :members:
   :undoc-members:
   :show-inheritance:

source.summarizer module
------------------------

.. automodule:: source.summarizer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: source
   :members:
   :undoc-members:
   :show-inheritance:
