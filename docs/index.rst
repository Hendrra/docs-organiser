.. Docs Organiser documentation master file, created by
   sphinx-quickstart on Sat Aug 20 12:25:07 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Docs Organiser's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
