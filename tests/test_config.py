import os.path

import pytest

from source.config import DIRECTORY_DOCUMENTS
from source.config import DIRECTORY_DOWNLOADS
from source.config import DIRECTORY_LOGGING
from source.config import PATTERNS
from source.config import SAVING_CONFIGURATION


# the tests below check whether the config file has been filled properly


@pytest.mark.parametrize(
    'variable', [
        DIRECTORY_DOWNLOADS,
        DIRECTORY_DOCUMENTS,
        DIRECTORY_LOGGING,
    ]
)
def test_filled_parameters_which_stores_directories(variable):
    assert len(variable) > 0


@pytest.mark.parametrize(
    'variable', [
        DIRECTORY_DOWNLOADS,
        DIRECTORY_DOCUMENTS,
        DIRECTORY_LOGGING,
    ]
)
def test_directories_exists(variable):
    assert os.path.exists(variable)


def test_filled_patterns():
    assert len(PATTERNS.keys()) > 0
    assert all([len(value) > 0 for value in PATTERNS.values()])


def test_filled_saving_configuration():
    for configuration in SAVING_CONFIGURATION:
        assert len(configuration.keys()) > 0
        assert all([len(value) > 0 for value in configuration.values() if value])
