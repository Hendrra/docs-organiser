# Docs Organiser

## Purpose
This code was created to make my life easier. I have several invoices each month and several billings. It's so annoying
to put them inside proper folder manually each time. That's why I decided to create a tool which will automate the
boring job for me! This tool is capable of crawling through the files inside a directory, choosing the proper ones and
moving them into their destination folder.

Of course one of the purposes of this project is to extend my portfolio.

## Installation
There are no special needs or requirements to install this tool.

To use the code provided here you just need to:
1. clone this repository using `git clone`,
2. install all the requirements using `pip install -r requirements.txt` from the parent directory.

Please note that I used Python 3.10.6 during the development however all versions 3.6+ will be suitable.

### Configuration of your tool
To use this code you need to work on the `source/config.py`. There are several variables which should be filled:
1. `DIRECTORY_DOWNLOADS`: the path to your directory where you store the downloaded files,
2. `DIRECTORY_DOCUMENTS`: the path to your directory where you store your documents,
3. `PATTERNS`: regex patterns which will be matches with the content of your documents, please see the example,
4. `SAVING_CONFIGURATION`: list of dictionaries with the details of your files, each dictionary should include the following keys: `name`, `type`, `folder_name` and `filename`. For details please see the `todo` in `source.config.py`.

You might also want to configure your own mappings between the names of the months, files, etc. Please see `POL2ENG`, `ENG2POL` or `NUMBERS2MONTHS`. It will be quite easy to add any new mapping.

### Testing the configuration
When you complete filling the config file you may want to run the following command from the parent directory.
```
pytest tests/
```
The tests are able to check whether your config file is filled properly.

### Using the tool
Now it's time to clean your `Downloads` directory. You just need to type in your terminal window
the following command.
```
python main.py
```

### Reading the output
You will see some output in the terminal window, there are two types of messages:
1. `[ INFO ] no filed detected.` -- means that there are no files to interpret,
2. `[ INFO ] working on <path to the file>` -- means that some proper file has been found,
3. `[ INFO ] insight found: <dictionary>` -- found details from the file,
4. `[ INFO ] copying file <source path> -> <target path>` -- information that the file was moved to its destination.

You will also be able to see the `.log` file in the `logs` directory. The logs provide additional information if 
there's a warning -- the number of the account which is in the invoice and the proper number.

### Additional parameters
You can also run the tool with some additional flags:
1. `--no-print` no output will be printed in the terminal window.

For more details please run `python main.py --help` in the parent directory.

### Examples
To make the cleaning of your `Downloads` directory just run the following command.
```
python main.py
```

To make the cleaning of your `Downloads` directory and without printing the output in the terminal just the following
command.
```
python main.py --no-print
```

## Support
If you have any questions about this code feel free to email me @dawidhanrahan@gmail.com.

## License
[MIT](https://gitlab.com/Hendrra/docs-organiser/-/blob/main/LICENSE)

## Project status
The project is completed and no further work is planned.
