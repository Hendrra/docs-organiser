import PyPDF2


class Extractor:
    """
    Extracts the data from a .pdf file.

    The Extractor contains the following public methods:
        1) `extract` -- does the extraction.
    """

    @classmethod
    def extract(cls, path):
        """
        Extracts the information stored in the .pdf file.

        Args:
            path (str): the full path to the .pdf file which is going to
            be extracted.

        Returns:
            str: the content of the file if exists, None otherwise.
        """
        with open(path, 'rb') as f:
            reader = PyPDF2.PdfReader(f)
            if reader.isEncrypted:
                print(f'[ WARNING ] {path} is encrypted! Continuing...')
                content = None
            else:
                page = reader.getPage(0)
                content = page.extractText()
            return content
