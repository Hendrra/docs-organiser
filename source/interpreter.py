import re

from source.config import PATTERNS
from source.config import POL2ENG


class Interpreter:
    """
    Interprets the content of extracted files. Searches for patterns
    such as date or the name of the company.

    The Interpreter has the following public methods:
        1) `interpret` -- iterates over patterns from the config file and
        looks for data.
    """

    @classmethod
    def interpret(cls, content):
        """
        Creates a dictionary. Uses the patterns and their names provided inside
        the config file.

        Args:
            content (str): the content of a .pdf file (for more details see Extractor).

        Returns:
            dict: dictionary containing the interpreted values from the patters.
        """

        insight = {}
        for name, pattern in PATTERNS.items():
            name = name.split('_')[0]
            value = cls._interpret_value(content=content, pattern=pattern)
            if not insight.get(name):
                try:
                    value = POL2ENG[value]
                except KeyError:
                    pass
                insight[name] = value
        return insight

    @classmethod
    def _interpret_value(cls, content, pattern, group_index=1):
        """
        Does the interpretation, i.e. searches for a given pattern inside
        the content. Tries to extract the data.
        """
        m = re.search(pattern, content)
        try:
            value = m.group(group_index)
        except AttributeError:
            value = None
        return value
