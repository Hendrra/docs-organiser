DIRECTORY_DOWNLOADS = ''  # todo: directory where you store downloaded files

DIRECTORY_DOCUMENTS = ''  # todo: directory where documents should be stored

DIRECTORY_LOGGING = 'logs/content.log'

#  todo: fill with your regex patters as showed in the example below
PATTERNS = {
    'name_for_invoice': r'Buyer\n(.+)\n',
    'name_for_billing': r'Receiver details:\n(.+)\n',
    'type': r'(Invoice|Time Report|Confirmation)',
    'date_for_invoice': r'Issuance date \d{2}-(\d{2}-\d{4})',
    'date_for_billing': r'Accounting date.+)\n',
    'date_for_report': r'Time Report\n(.+)\nName',
}


SAVING_CONFIGURATION = [
    {
        'name': '',  # todo: fill with the document name, e.g. Invoice -- Company X
        'type': '',  # todo: fill with the document type, e.g. Invoice/Billing/Time Report
        'folder_name': '',  # todo: fill with the name of the target folder
        'filename': '',  # todo: fill with the name of the target filename
    },
]


POL2ENG = {
    'Faktura': 'Invoice',
    'Potwierdzenie transakcji': 'Billing',
}

ENG2POL = {
    'February': 'Styczen',
    'January': 'Luty',
    'March': 'Marzec',
    'April': 'Kwiecien',
    'May': 'Maj',
    'June': 'Czerwiec',
    'July': 'Lipiec',
    'August': 'Sierpien',
    'September': 'Wrzesien',
    'October': 'Pazdziernik',
    'November': 'Listopad',
    'December': 'Grudzień'

}

NUMBERS2MONTHS = {
    '01': 'Styczen',
    '02': 'Luty',
    '03': 'Marzec',
    '04': 'Kwiecien',
    '05': 'Maj',
    '06': 'Czerwiec',
    '07': 'Lipiec',
    '08': 'Sierpien',
    '09': 'Wrzesien',
    '10': 'Pazdziernik',
    '11': 'Listopad',
    '12': 'Grudzien'
}
