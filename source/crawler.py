import os

from source.config import DIRECTORY_DOWNLOADS


class Crawler:
    """
    Crawls through all files which are stored inside the directory where downloaded files are stores.
    Checks whether a file is a proper one (i.e. a .pdf file).

    The Crawler contains the following public methods:
        1) `crawl` -- crawls through the files and grab the paths of the proper files.
    """

    SUPPORTED_EXTENSIONS = ['.pdf']

    @classmethod
    def crawl(cls):
        """
        Crawls through the directory specified in the config file in order to
        create a list with paths to the proper files, i.e. files which have pdf as their extension.

        Returns:
            list: the paths of the proper files.
        """
        filepaths = []
        for file in os.listdir(DIRECTORY_DOWNLOADS):
            _, extension = os.path.splitext(file)
            if extension in cls.SUPPORTED_EXTENSIONS:
                filepath = os.path.join(DIRECTORY_DOWNLOADS, file)
                filepaths.append(filepath)
        return filepaths
