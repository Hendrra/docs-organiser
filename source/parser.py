import argparse


def parse_arguments():
    """
    Creates the parser.
    """
    parser = argparse.ArgumentParser(description='Argument parser for the Docs Organiser.')

    help_message_for_print = 'Argument which indicates whether the program should print the results. ' \
                             'By default it displays the result.'
    parser.add_argument(
        '--print',
        help=help_message_for_print,
        action=argparse.BooleanOptionalAction,
        required=False,
        default=True,
    )

    return parser.parse_args()
