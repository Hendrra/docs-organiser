import logging
import os.path
import shutil

from source.crawler import Crawler
from source.extractor import Extractor
from source.interpreter import Interpreter
from source.config import DIRECTORY_LOGGING
from source.summarizer import Summarizer


logging.basicConfig(
    filename=DIRECTORY_LOGGING,
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(message)s',
)


class Organiser:
    """
    Main class. Does the cleaning of the `Download` directory. This is:
        1) finds whether any invoices are in the directory,
        2) checks what kind of invoice is it,
        3) finds whether any time report is in the directory,
        4) finds whether any billings are in the directory,
        5) moves the files to correct folders.

    The Organiser contains the following public methods:
        1) `organise` -- does the jobs described above.
    """

    def __init__(self, parsed, crawler=Crawler, extractor=Extractor, interpreter=Interpreter, summarizer=Summarizer):
        """
        Initialises the Organiser.

        Args:
            parsed (argparse.Namespace): parsed arguments in the form provided by argparse, for now just one parameter
            is available -- this is the print parameter which is capable of printing the output in the terminal window,
            crawler (Crawler): the Crawler class, used for crawling through the directory,
            extractor (Extractor): the Extractor class, used for extracting the data from a single .pdf file,
            interpreter (Interpreter): the Interpreter class, used for extracting information from .pdf file,
            summarizer (Summarizer): the Summarizer class, used for depicting the target path.
        """
        self._print = parsed.print

        self._crawler = crawler
        self._extractor = extractor
        self._interpreter = interpreter
        self._summarizer = summarizer

    def organise(self):
        """
        Organises the files in the `Download` directory.
        For details of the process please see the `content.log` file inside the log directory.
        """
        paths = self._crawler.crawl()

        if not paths:
            logging.info('No files detected.')
            if self._print:
                print('[ INFO ]: no files detected.')

        for path in paths:
            logging.info(f'Working on {path}')
            if self._print:
                print(f'[ INFO ]: working on {path}')

            content = self._extractor.extract(path=path)
            insight = self._interpreter.interpret(content=content)
            logging.info(f'Insight found: {insight}')
            if self._print:
                print(f'[ INFO ]: insight found: {insight}')

            summary = self._summarizer(insight=insight, path=path).summarize()
            source_path, target_path = summary['source_path'], summary['target_path']

            if not target_path:
                logging.warning('No target path found!')
                if self._print:
                    print('[ WARNING ]: no target path found!')
            else:
                self._move_file(source_path=source_path, target_path=target_path)

                logging.info(f'Copying: {source_path} -> {target_path}')
                if self._print:
                    print(f'[ INFO ]: copying file {source_path} -> {target_path}')

    @staticmethod
    def _move_file(source_path, target_path):
        """
        Moves file from source path to target path. If the latter does not exist the
        function creates a proper directory.
        """
        directory = os.path.dirname(target_path)

        if not os.path.exists(directory):
            logging.info(f'Created a new directory: {directory}')
            os.mkdir(directory)

        shutil.move(src=source_path, dst=target_path)
