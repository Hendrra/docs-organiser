import abc
import importlib
import os
import re

from source.config import DIRECTORY_DOCUMENTS
from source.config import ENG2POL
from source.config import NUMBERS2MONTHS
from source.config import SAVING_CONFIGURATION


class Summarizer:
    """
    Creates the summary of the inspected file, i.e. creates the string which contains the
    target/destination path for a given file.

    This class has the following public methods:
        1) `summarize` -- in fact the summarize method is a factory which chooses the proper class
        to do the job.
    """

    def __init__(self, insight, path):
        """
        Initialises the Summarizer class.

        Args:
            1) insight (dict): dictionary containing the insights from the file, this is the most important information
            extracted from a .pdf file,
            2) path (str): source path of the file.
        """
        self._type = insight['type']
        self._name = insight['name']
        self._date = insight['date']
        self._path = path

    def summarize(self):
        """
        Finds the proper destination path for a file using the insights.

        Returns:
            dict: a dictionary with two keys:
                1) `source_path` -- the source path of the file,
                2) `target_path` -- the target path or destination path for the given file.
        """
        class_ = self._get_summary_class()
        summary = class_(type_=self._type, name_=self._name, date_=self._date).get_summary()
        summary['source_path'] = self._path
        return summary

    def _get_summary_class(self):
        """
        Factory which chooses the proper class to make to summary.

        Returns:
            SummarizerForAttribute: a child instance of the class.
        """
        module = importlib.import_module(__name__)
        attr = self._type.replace(' ', '')
        class_ = getattr(module, 'SummarizerFor' + attr)
        return class_


class SummarizerForAttribute(metaclass=abc.ABCMeta):
    """
    Base class for all summarizers.

    If you need to create a new summarizer all you need to do is to implement the `get_date`
    method. The rest of information should be computed properly using the included methods, no
    changes are required.

    The child classes will have the following public methods:
        1) `get_summary` -- function which returns a dictionary with one key: `target_path`.
    """

    def __init__(self, type_, name_, date_):
        """
        Initialises the Summarizer.

        Args:
            type_ (str): the type of the document, e.g. "Invoice",
            name_ (str): the name of the company,
            date_ (str): the date, probably each type will have a different format.
        """
        self._type = type_
        self._name = name_
        self._date = date_
        self._summary = {}

    def get_summary(self):
        """
        Computes the proper `target_path`.

        Returns:
            dict: dictionary with the target path stored under `target_path` key.
        """
        target_path = self._get_target_path()
        self._summary['target_path'] = target_path
        return self._summary

    def _get_target_path(self):
        """
        Prepares the target path. The target path consists of the following information:
            1) the main directory where the documents are stored,
            2) the folder name which is calculated,
            3) the folder date which is calculated,
            4) the filename.

        Returns:
            str: the target path.
        """
        folder_name = self._get_folder_name()
        folder_date = self._get_date()
        filename = self._get_filename()
        target_path = os.path.join(
            *[DIRECTORY_DOCUMENTS, folder_name, folder_date, filename]
        )
        return target_path

    def _get_folder_name(self):
        """
        Extracts the name of the folder.
        """
        folder_name = ''
        for item in SAVING_CONFIGURATION:
            if item.get('name') == self._name:
                folder_name = item['folder_name']
        return folder_name

    @abc.abstractmethod
    def _get_date(self):
        """
        Extracts the date. This method is to be implemented in the child classes.
        """
        pass

    def _get_filename(self):
        """
        Extracts the name of the file.
        """
        filename = ''
        for item in SAVING_CONFIGURATION:
            if item.get('name') == self._name:
                filename = item['filename']
        return filename


class SummarizerForInvoice(SummarizerForAttribute):
    """
    Summarizer for invoices.
    """

    def _get_date(self):
        month, year = self._date.split('-')
        month = NUMBERS2MONTHS[month]
        return '{month} {year}'.format(month=month, year=year)


class SummarizerForTimeReport(SummarizerForAttribute):
    """
    Summarizer for time reports. Now supports only one reporting tool.
    """

    def _get_date(self):
        pattern = r'(.+) \d{2}, (\d{4}) -'
        m = re.search(pattern, self._date)
        try:
            month, year = m.group(1), m.group(2)
        except AttributeError:
            raise Exception('Cannot extract the date from TimeReport.')
        month = ENG2POL[month]
        return '{month} {year}'.format(month=month, year=year)


class SummarizerForBilling(SummarizerForAttribute):
    """
    Summarizer for billings.
    """

    def _get_date(self):
        _, month, year = self._date.split('.')
        month = NUMBERS2MONTHS[month]
        return '{month} {year}'.format(month=month, year=year)
